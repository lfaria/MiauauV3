package miauauv3

import geb.spock.GebSpec
import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole
import miauau.br.com.miauau.rf03.Duvida
import miauau.rf01.Aviso
import miauau.rf01.Imagem
import org.springframework.beans.factory.annotation.Autowired

@Integration
@Rollback
class DuvidaSpec extends GebSpec {
	def user1
	def duvida
	@Autowired
	SpringSecurityService springSecurityService

	def setup() {
		user1 = new User(username: 'testUser', email: 'testUserEmail@email.com', password: '123')
		user1.save(failOnError: true, flush: true)
		UserRole.create(user1, Role.findByAuthority("ROLE_ADMIN"), true)

	}

	def doLogin() {
		go "/login/auth"
		$("input[name='username']").value('testUser')
		$("input[name='password']").value('123')
		$("#submit").click()
	}

	def cleanup() {
		UserRole.removeAll(user1)
		Aviso.findAllByUsuario(user1)*.delete()
		user1.delete(flush: true)

	}

	def "criando uma duvida só com email"() {
		when: 'vai em duvidas'
		go "/duvida/index"
		and: 'preenche somente o campo email'
		$('input[name="email"]').value("testEmail@email.com")
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == 'duvida/index'

	}

	def "criando uma duvida só com email e mensagem"() {
		when: 'vai em duvidas'
		go "/duvida/index"
		and: 'preenche somente o campo email e mensagem'
		$('input[name="email"]').value("testEmail@email.com")
		$('textarea[name="mensagem"]').value("Mensagem de duvida")
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == 'duvida/index'
	}

	def "criando uma duvida só com mensagem"() {
		when: 'vai em duvidas'
		go "/duvida/index"
		and: 'preenche somente o campo mensagem'
		$('textarea[name="mensagem"]').value("Mensagem de duvida")
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == 'duvida/index'
	}

	def "criando uma duvida completa"() {
		when: 'vai em duvidas'
		go "/duvida/index"
		and: 'preenche todos os campos'
		$('input[name="email"]').value("testEmail@email.com")
		$('input[name="nome"]').value("test nome")
		$('textarea[name="mensagem"]').value("Mensagem de duvida")
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == 'duvida/save'
	}

	def "respondendo uma duvida sem dar resposta"() {
		given:
		doLogin()
		when: 'vai em duvidas para admin'
		go "/duvida/duvidas"
		and: "seleciona uma duvida e clica em responder"
		$("tbody tr td a")[0].click()
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == "duvida/responder/1" as String
	}

	def "respondendo uma duvida"() {
		given:
		doLogin()
		when: 'vai em duvidas para admin'
		go "/duvida/duvidas"
		and: "seleciona uma duvida e clica em responder"
		$("tbody tr td a")[0].click()
		$("textarea[name='resposta']").value("Resposta da duvida é 4")
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == "duvida/duvidas"
	}

	def "respondendo e fixando uma duvida"() {
		given:
		doLogin()
		when: 'vai em duvidas para admin'
		go "/duvida/duvidas"
		and: "seleciona uma duvida e clica em responder"
		$("tbody tr td a")[0].click()
		$("textarea[name='resposta']").value("Resposta da duvida é 4")
		$("div.checkbox").click()
		$("button[name='submitButton']").click()
		go "/duvida/index"
		then:
		$("div[name='duvidasFrequentes'] > div.row").size() == 1
	}

	def "excluindo uma duvida"(){
		given:
		doLogin()
		when: 'vai em duvidas para admin'
		go "/duvida/duvidas"
		and: "seleciona uma duvida e clica em responder"
		withConfirm(true){$('tbody tr td a')[1].click()}
		then:
		currentUrl.split("/")[3..-1].join("/") == "duvida/duvidas"
		$("tr").size() == 1
	}
}
