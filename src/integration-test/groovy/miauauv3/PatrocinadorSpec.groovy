package miauauv3

import geb.spock.GebSpec
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole
import miauau.rf07.Patrocinador

@Integration
@Rollback
class PatrocinadorSpec extends GebSpec {
	def user1

	def setup() {
		user1 = new User(username: 'testUser', email: 'testUserEmail@email.com', password: '123')
		user1.save(failOnError: true, flush: true)
		UserRole.create(user1, Role.findByAuthority("ROLE_ADMIN"), true)

	}

	def doLogin() {
		go "/login/auth"
		$("input[name='username']").value('testUser')
		$("input[name='password']").value('123')
		$("#submit").click()
	}

	def cleanup() {
		UserRole.removeAll(user1)
		Patrocinador.list()*.delete()
		user1.delete(flush: true)

	}
	def cadastraPatrocinador(){
		doLogin()
		go "/patrocinador/form"
		$("form").nome = "nit"
		$("form").email = "nit@unifei.edu.br"
		$("form").telefone = "36227889"
		$('form').featuredImageFile = "/home/lfaria/Documentos/95536a31-eb2a-413a-988c-8015994ec4c0.jpeg"
		$('button[name="submitButton"]').click()


	}

	def "cadastrando patrocinador"() {
		when:
			cadastraPatrocinador()
		then:
			currentUrl.split("/")[3..-1].join("/") == "patrocinador/index"
	}

	def "editando patrocinador" () {
		given:
			cadastraPatrocinador()
		when:
			go "/patrocinador/form/${Patrocinador.first().id}"
			$("form").nome = "naum é mais u niti"
			$("form").email = "nit@unifei.edu.br"
			$('button[name="submitButton"]').click()
		then:
			currentUrl.split("/")[3..-1].join("/") == "patrocinador/index"

	}

	def "excluindo patrocinador"() {
		given:
			cadastraPatrocinador()
		when:
			withConfirm(true) {$('tbody tr td a')[1].click()}
		then:
			currentUrl.split("/")[3..-1].join("/") == "patrocinador/index"

	}

}
