package miauauv3

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.integration.Integration
import grails.transaction.*
import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole
import miauau.rf01.Aviso
import miauau.rf01.Imagem
import org.springframework.beans.factory.annotation.Autowired
import geb.spock.*

/**
 * See http://www.gebish.org/manual/current/ for more instructions
 */
@Integration
@Rollback
class AvisoSpec extends GebSpec {
    def user1
    def user2
    @Autowired
    SpringSecurityService springSecurityService
    def setup() {
        user1 = new User(username: 'ntestUser', email:'testssUserEmail@email.com', password:'123')
        user1.save(failOnError:true, flush: true)
        UserRole.create(user1, Role.findByAuthority("ROLE_ADMIN"), true)
    }
    def doLogin(){
        go "/login/auth"
        $("input[name='username']").value('ntestUser')
        $("input[name='password']").value('123')
        $("#submit").click()
    }

    def cleanup() {
        UserRole.removeAll(user1)
        Aviso.findAllByUsuario(user1)*.delete()
        user1.delete(flush: true)

    }

    def saveAviso() {
        $("a[href='/aviso/meusAvisos']").click()
        $('a[href="/aviso/form"]').click()
        $('form').featuredImageFile = "/home/lfaria/Desktop/Faculdade/6_periodo/Adler/material-dashboard-html-v1.1.1/assets/img/new_logo.png"
        $('form').mensagem = "Mensagem de teste\r\n Mais aguma coisa\r\nMensagem de teste\r\n Mais aguma coisa\r\n"

        $('button[name="loadButton"]').click()
        waitFor { $('button[name="uploadButton"]')}
        $('button[name="uploadButton"]').click()
    }

    void "testando cadastro de aviso"() {
        when:"Quando clica no menu avisos"
            doLogin()
            saveAviso()
        then:"Pagina meus avisos"
        	currentUrl.split("/")[3..-1].join("/") == "aviso/meusAvisos"
            $("tr").size() == 2
    }

    void "testando editar aviso"() {
        when:
            doLogin()
            saveAviso()
            $('tbody tr td a')[0].click()
        $('form').featuredImageFile = "/home/lfaria/Desktop/Faculdade/6_periodo/Adler/material-dashboard-html-v1.1.1/assets/img/apple-icon.png"
        $('form').mensagem = "Mensagem de teste modificada\r\n Mais aguma coisa modificada\r\nMensagem de teste modificada\r\n Mais aguma coisa modificada\r\n"
        $('button[name="loadButton"]').click()
        waitFor { $('button[name="uploadButton"]')}
        $('button[name="uploadButton"]').click()

        then:
            currentUrl.split("/")[3..-1].join("/") == "aviso/meusAvisos"
            $('tbody tr td')[1].text().substring(0, 80) != Aviso.findByUsuario(user1).mensagem.substring(0, 80)
    }

    void "testando excluir aviso"() {
        when:
            doLogin()
            saveAviso()
            withConfirm(true) {$('tbody tr td a')[1].click()}
        then:
            currentUrl.split("/")[3..-1].join("/") == "aviso/meusAvisos"
            $("tr").size() == 1

    }
    void "testando cancelar excluir aviso"() {
        when:
        doLogin()
        saveAviso()
        withConfirm(false) {$('tbody tr td a')[1].click()}
        then:
        currentUrl.split("/")[3..-1].join("/") == "aviso/meusAvisos"
        $("tr").size() == 2

    }
}
