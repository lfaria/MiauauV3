package miauauv3

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.integration.Integration
import grails.transaction.*
import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole
import org.springframework.beans.factory.annotation.Autowired
import spock.lang.*
import geb.spock.*

/**
 * See http://www.gebish.org/manual/current/ for more instructions
 */
@Integration
@Rollback
class AuthSpec extends GebSpec {
    def user1
    def user2
    @Autowired
    SpringSecurityService springSecurityService

    def setup() {
		user1 = new User(username: 'adminUser', email:'testUserEmail@email.com', password:'123')
		user1.save(failOnError:true, flush: true)
        user2 = new User(username: 'outroTest', email:'outroTestEmail@email.com', password:'123')
		user2.save(failOnError:true, flush: true)

		def role = Role.get(1)
		UserRole.create(user1, role)
		UserRole.create(user2, Role.findByAuthority('ROLE_USER'))
    }

    def cleanup() {
		user1 = User.get(user1.id)
		user2 = User.get(user2.id)
		if(user1) {
			UserRole.removeAll(user1)
			user1.delete(flush: true)
		}
		if(user2) {
			UserRole.removeAll(user2)
			user2.delete(flush: true)
		}
    }

	def doLogin(String username = null, String password = null) {
		go '/login/auth'
		$("input[name='username']").value(username?: 'adminUser')
		$("input[name='password']").value(password?: '123')
		$("#submit").click()
	}
    void "testando logar com usuario"() {
        when:"Entra no site"
            doLogin()
        then:"Deve estar na home"
            currentUrl.split("/").size() == 3

    }
    void "testando editar usuario"() {
        given:
            doLogin()
        when:'Vai para pagina de editar usuário'
            go '/user/form/'+user2.id
            $("input[name='username']").value('novoUsername')
            $("button[name='loadForm']").click()
		and:
			go'/logoff'
			doLogin('outroTest')
        then: 'nao deve estar na home'
			currentUrl.split("/").size() != 3

    }

	void "testando editar usuario como administrador"(){
		given: 'usuario a ser editado e feito login'
			String userName = user2.username
			doLogin()
		when: 'vai para pagina de editar usuario'
			go"/user/index"
			$("a[href='/user/form/${user2.id}'").click()
			$("form").email = "EMAILLLLLLLLLLLLLL@email.com"
			$("input[name='username']").value('Editei voce hahaha')
			$("button[name='loadForm']").click()

		then:
			User.get(user2.id).username != userName
			currentUrl.split("/")[3..-1].join("/") == 'user/index'
	}
	void "testando excluir usuario"() {
		given: 'estar logado e quantidade de usuarios'
			int quantidade = User.count()
			doLogin()
		when: 'clica em excluir e vai para pagina de exclusao'
			go"/user/index"
			withConfirm(true) {$('tbody tr td a')[1].click()}
		then:
			$("tbody tr").size() == quantidade - 1
	}

	void "testando criar usuario"() {
		when:
			go "/user/form"
			$("form").username = "novoUser"
			$("form").email = "email@email.com"
			$("form").password = "123"
			$("form").passwordCheck = "123"
			$("button[name='loadForm']").click()
		and:
			$("form").username = "novoUser"
			$("form").password = "123"
			$("#submit").click()
		then:
			currentUrl.split("/").size() == 3
	}

	void "testando adicionar 2 roles ao usuario"(){
		given:
			doLogin()
		when: 'vai para pagina de editar usuario'
			go"/user/index"
			go"/user/form/${user2.id}"
			$("button[name='role0']").click()
			$("button[name='role2']").click()
			$("button[name='loadForm']").click()
		then:
			currentUrl.split("/")[3..-1].join("/") == 'user/index'

	}

}
