package miauauv3

import geb.spock.GebSpec
import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole
import miauau.rf05.Noticia
import org.springframework.beans.factory.annotation.Autowired

@Integration
@Rollback
class NoticiaSpec extends GebSpec {
	def user1
	@Autowired
	SpringSecurityService springSecurityService

	def setup() {
		user1 = new User(username: 'adminUser', email:'testUserEmail@email.com', password:'123')
		user1.save(failOnError:true, flush: true)

		def role = Role.get(1)
		UserRole.create(user1, role)
	}

	def cleanup() {
		user1 = User.get(user1.id)
		if(user1) {
			UserRole.removeAll(user1)
			Noticia.findAllByUsuario(user1)*.delete()
			user1.delete(flush: true)
		}
	}

	def doLogin(String username = null, String password = null) {
		go '/login/auth'
		$("input[name='username']").value(username?: 'adminUser')
		$("input[name='password']").value(password?: '123')
		$("#submit").click()
	}
	def cadastraNoticia(){
		doLogin()
		go "/noticia/form"
		$("form").titulo = "Mulher que matou 37 animais é condenada a mais de 17 anos de prisao"
		$('form').featuredImageFile = "/home/lfaria/Desktop/mulher-que-matou-37-animais-e-condenada-a-mais-de-17-anos-de-prisao-pdd1.jpeg"
		$("div.checkbox").click()
		js.exec('CKEDITOR.instances["ckeditor"].insertHtml("Em uma decisão histórica, a justiça brasileira condenou uma mulher a mais de 17 anos de reclusão por crimes de maus-tratos a animais. A assassina de cães e gatos teve sua prisão decretada na última sexta-feira, dia 10 de novembro." +\n' +
				'\t\t\t\t\t"" +\n' +
				'\t\t\t\t\t"Dalva Lina da Silva, de 48 anos, mora na Vila Mariana, em São Paulo, e era conhecida por moradores da região como protetora de animais. Muitas pessoas levavam cães e gatos abandonados para a casa de Dalva, para que ela ajudasse a encontrar novas famílias para os bichinhos. Porém, não era isso o que a mulher, que todos acreditavam ser uma boa pessoa, fazia.A mulher parecia encontrar adotantes para os animais de uma forma tão rápida que a ONG Adote um Gatinho desconfiou de que alguma coisa estava errada e contratou um detetive particular, que descobriu o que Dalva realmente fazia quando flagrou a falsa protetora colocando vários sacos de lixo do lado de fora de sua casa. Ao averiguar o que continha dentro dos sacos, o detetive encontrou vários corpos de animais mortos.\\n" +\n' +
				'\t\t\t\t\t"\\n" +\n' +
				'\t\t\t\t\t"De acordo com perícia realizada nos corpos dos cães e gatos, que tinham várias perfurações na região do peito, a mulher injetava uma droga no coração dos animais. Porém, para acertar o alvo ela fazia várias tentativas em cada bichinho, o que trazia muita dor e sofrimento para eles.\\n" +\n' +
				'\t\t\t\t\t"\\n" +\n' +
				'\t\t\t\t\t"Em 2015, a assassina de animais teve sua primeira condenação pelo crime de maus-tratos. Na época, Dalva foi sentenciada a 12 anos, seis meses e 14 dias de prisão e estava cumprindo a pena em liberdade.\\n" +\n' +
				'\t\t\t\t\t"\\n" +\n' +
				'\t\t\t\t\t"Porém, o Ministério Público Estadual recorreu solicitando aumento da pena e obteve, na última quinta-feira, 9 de novembro, decisão favorável da 10ª Câmara Criminal do Tribunal de Justiça, que negou a apelação da defesa contra a condenação. Com isso, a pena de Dalva Lina da Silva aumentou para 17 anos, 6 meses e 26 dias de reclusão em regime semiaberto por crime de maus-tratos.");')
		$("button[name='submitButton']").click()
	}

	def "cadastrando noticia"(){
		given:
			doLogin()
		when:
			go "/noticia/form"
			cadastraNoticia()
			go "/noticia"
		then:
			true
	}

	def "excluidoNoticia"(){
		given:
			cadastraNoticia()
		when:
			go "/noticia"
			withConfirm(true) {$('tbody tr td a')[1].click()}
		then:
			currentUrl.split("/")[3..-1].join("/") == "noticia/index"
			$("tr").size() == 1

	}

	def "comentando noticia"(){
		given:
		cadastraNoticia()
		when:
		go "/noticia/visualizar/${Noticia.first().id}"
		$("form").mensagem = "Nossa gente kiabosurdo....."
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == "noticia/visualizar/${Noticia.first().id}"
	}

	def "excuindo comentario"() {
		given:
		cadastraNoticia()
		when:
		go "/noticia/visualizar/${Noticia.first().id}"
		$("form").mensagem = "Nossa gente kiabosurdo....."
		$("button[name='submitButton']").click()
		and:
		$('#delete')[1].click()
		then:
		currentUrl.split("/")[3..-1].join("/") == "noticia/visualizar/${Noticia.first().id}"
	}

	def "editando comentario" (){
		given:
		cadastraNoticia()
		when:
		go "/noticia/visualizar/${Noticia.first().id}"
		$("form").mensagem = "Nossa gente kiabosurdo....."
		$("button[name='submitButton']").click()
		and:
		$('#editar').click()
		$("form").mensagem = "Editei ese comentauro"
		$("button[name='submitButton']").click()
		then:
		currentUrl.split("/")[3..-1].join("/") == "noticia/visualizar/${Noticia.first().id}"
	}
}
