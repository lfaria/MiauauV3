import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.htmlunit.HtmlUnitDriver

/* Implicit waiting timings of any element */
waiting {
    timeout = 10
    retryInterval = 0.5
}

environments {

    htmlUnit {
        driver = { new HtmlUnitDriver() }
    }

    chrome {
        driver = { new ChromeDriver() }
    }

    firefox {
        driver = { new FirefoxDriver() }
    }

}