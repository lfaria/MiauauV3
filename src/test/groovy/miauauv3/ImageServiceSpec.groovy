package miauauv3

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import miauau.auth.User
import miauau.rf01.Imagem
import org.grails.plugins.testing.GrailsMockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

@TestFor(ImageService)
@Mock([Imagem, User])
class ImageServiceSpec extends Specification {
    def "testando metodo de salvar imagem" (){
        given: 'um arquivo multipart e um usuario'
            def imgFile = new File("/home/lfaria/miauauImages/images/Captura de tela de 2017-10-20 19-11-57.png")
            MultipartFile mpFile = new GrailsMockMultipartFile("img", imgFile.bytes)
            User user = new User(username: 'adasd', password: '123')
            user.save()
        when:
            Imagem img = service.saveImage(mpFile, user)
        then:
            img.usuario == user
            img.bytes == imgFile.bytes
    }
}
