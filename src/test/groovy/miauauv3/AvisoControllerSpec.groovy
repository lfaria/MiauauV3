package miauauv3

import grails.plugin.springsecurity.SpringSecurityService
import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import miauau.auth.User
import miauau.rf01.Aviso
import miauau.rf01.Imagem
import org.grails.plugins.testing.GrailsMockMultipartFile
import org.springframework.web.multipart.MultipartFile
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(AvisoController)
@Mock([Aviso, User, Imagem])
class AvisoControllerSpec extends Specification {
    def springSecurityService = Mock(SpringSecurityService)
    def setup() {
        new User(username: 'lf', email:'novoEmail',password: '123').save()
        new User(username:'outro', email:"novoEmail2",password:'123').save()

    }

    def cleanup() {
        User.list().each{
            it.delete()
        }
    }

    void "test meus avisos"() {
        given:
            User usuario = User.get(1)
            User outroUser = User.get(2)
            springSecurityService.getCurrentUser() >> { return usuario }
            List<Aviso> avisoList = [
                    new Aviso(usuario: usuario, mensagem: 'Minha mensagem'),
                    new Aviso(usuario: usuario, mensagem: 'Minha mensagem 2'),
                    new Aviso(usuario: outroUser, mensagem: 'Minha mensagem 3'),
                    ]
            avisoList.each {
                it.save(failOnError: true, flush: true)
            }
        and: "injetando o spring security"
            controller.springSecurityService = springSecurityService
        when: 'carrega meus avisos'
            controller.meusAvisos()
        then: 'os avisos devem ser somente desse usuario'
            controller.springSecurityService.getCurrentUser() == usuario
            model.avisos.size() == 2
            model.avisos.find{it == avisoList.get(0)}
            model.avisos.find{it == avisoList.get(1)}
            model.avisos.find{it != avisoList.get(2)}

    }

    def "testando view form quando nao é passado parametro"() {
        when: "chama a view"
            controller.form()
        then: 'aviso deve ser null'
            model.aviso == null
            view == "/aviso/form"
    }

    def "testando view form quando é passado um aviso"() {
        given:
            Aviso v = new Aviso(mensagem: 'minha msg', usuario: User.get(1))
        when: "chama a view"
            controller.form(v)
        then: 'aviso deve existir na view'
            model.aviso != null
            view == "/aviso/form"
    }

}
