package miauau.rf07

import miauau.rf01.Imagem

class Patrocinador {
    String nome
    String email
    String telefone
    Imagem imagem

    static constraints = {
        email email: true
        imagem nullable: true
        telefone nullable: true
    }
}
