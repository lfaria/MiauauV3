package miauau.br.com.miauau.rf03

import miauau.auth.User

class Duvida {
    String mensagem
	String nome
	String email
    User usuario
	String resposta
	Date dataResposta
	boolean frequente = false
    static constraints = {
		dataResposta nullable: true
		frequente nullable: true
		resposta nullable: true
		usuario nullable: true
		email nullable: true
		mensagem length: 180
		nome nullable: true

    }
}