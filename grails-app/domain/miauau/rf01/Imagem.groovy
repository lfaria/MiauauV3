package miauau.rf01

import grails.util.Holders
import miauau.auth.User

class Imagem {
	String contentType
	byte[] bytes

	static transients = ['imageService']
	static constraints = {
	}
	static mapping = {
		autowire true
		bytes column: 'bytes', sqlType: 'longblob'
	}


	def getImage() {
		Holders.applicationContext.getBean("imageService").getImage(this.id)
	}

}
