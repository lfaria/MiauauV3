package miauau.rf01

import miauau.auth.User
import miauau.rf05.Resposta

class Aviso {
	String mensagem
	Imagem imagem
	static belongsTo = [usuario: User]
	static hasMany = [resposta: Resposta]

	static constraints = {
		imagem nullable: true, cascade: 'all-delete-orphan'
		mensagem length: 180
		resposta nullable: true, cascade: 'all-delete-orphan'
	}

	static mapping = {
		imagem cascade: 'all-delete-orphan'
	}
}
