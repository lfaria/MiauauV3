package miauau.rf05

import miauau.auth.User

class Resposta {
	String mensagem
	Date dateCreated
	Date lastUpdated

	static belongsTo = [usuario: User]

	static constraints = {
	}
}
