package miauau.rf05

import miauau.auth.User
import miauau.rf01.Imagem

class Noticia {
	String titulo
	String mensagem
	Date dateCreated
	Date lastUpdated
	Imagem imagem
	boolean paginaInicial = false
	static belongsTo = [usuario: User]
	static hasMany = [resposta: Resposta]

	static mapping = {
		mensagem type: 'text'
		imagem cascade: 'all-delete-orphan'
		resposta nullable: true, cascade: 'all-delete-orphan'
	}

	static constraints = {
		imagem nullable: true, blank: true

		usuario validator: { val, obj ->
			val.authorities.find {
				it.authority == "ROLE_ADMIN" || it.authority == "ROLE_SYSADMIN"
			} ? true : 'deveSerAdmin'
		}

		paginaInicial validator: { val, obj ->
			val ? findAllByPaginaInicial(true).size() <= 5 ?: 'maximo5' : !val
		}

	}
}
