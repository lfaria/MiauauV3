<div class="col-sm-11 col-sm-offset-1">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h3>Comentários</h3>
        </div>
        <div class="card-content">
            <g:each in="${conteudo.resposta.sort{a,b-> a.dateCreated<=>b.dateCreated}}" var="it" status="i" >
                <div class="row">
                    <div class="col-sm-11 col-sm-offset-1">
                        <div class="card card-stats">
                            <div class="card-header" data-background-color="orange">
                                <i class="material-icons">content_copy</i>
                                <p class=""></p>
                            </div>
                            <div class="card-content">
                                <p class="category">Usuario: ${it.usuario.email}</p>
                                <g:if test="${it.usuario.id == sec.loggedInUserInfo(field: 'id') as Long}">
                                    <a id="editar" href="#comentar" onclick="editComment(this,${it.id})"><i class="material-icons">edit</i></a>
                                    <g:link action="deletar" id="delete" controller="resposta" id="${conteudo.id}" params="${[isNoticia: isNoticia, noticiaId: it.id]}" onclick="return confirm('Tem certeza?');"><i class="material-icons">delete_forever</i></g:link>
                                </g:if>
                                ${it.dateCreated}

                            </div>
                            <div class="card-footer">
                                <h4 name="mensagem${it.id}">${it.mensagem}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </g:each>
            <g:form action="adicionarResposta" controller="resposta">
                <div id="comentar" class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Comentário</label>

                            <div class="form-group label-floating">
                                <label class="control-label">Escreva um comentário aqui</label>
                                <textarea class="form-control" name="mensagem" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="${isNoticia}" name="isNoticia">
                    <input type="hidden" value="${conteudo.id}" name="id">
                    <input type="hidden" value="" name="respostaId" />
                    <button class="btn btn-primary pull-right" type="submit" name="submitButton">Enviar</button>
                </div>
            </g:form>
        </div>
    </div>
</div>
<g:javascript>
    function editComment(whoIm, respostaId){
        $("textarea[name='mensagem']")[0].click();
        $("textarea[name='mensagem']").val($('h4[name="mensagem'+respostaId+'"]').text());
        $("input[name='respostaId']").val(respostaId);
    }

</g:javascript>