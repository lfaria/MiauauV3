<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Noticias</title>
</head>

<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Noticias do site</h4>
            <p class="category">Você pode cadastrar, editar, exluir ou deletar duvidas por aqui</p>
            <g:link action="form">
                <i class="material-icons">add</i>Cadastrar nova noticia</g:link>
        </div>

        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-primary">
                <th>Titulo</th>
                <th>Data Criada</th>
                <th>Ultima Edição</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>
                <tbody>
                <g:each in="${noticias}">
                    <tr>
                        <td>${it.titulo}</td>
                        <td>${it.dateCreated}</td>
                        <td>${it.lastUpdated}</td>
                        <td>
                            <g:link action="form" id="${it.id}">
                                <i class="material-icons">edit</i></g:link>

                        </td>
                        <td>
                            <g:link action="delete" id="${it.id}" onclick="return confirm('Tem certeza?');">
                                <i class="material-icons">delete forever</i></g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>