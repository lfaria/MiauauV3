<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Cadastrar Noticia</title>
    <ckeditor:resources/>
</head>

<body>
<g:form method="post" action="save" enctype="multipart/form-data">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">${(noticia && noticia.id) ? 'Edite essa noticia' : 'Cadastre uma noticia'}</h4>

                <p class="category">Preencha os campos</p>
            </div>

            <div class="card-content">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label">Titulo*</label>
                            <input class="form-control" value="${noticia?.titulo}" name="titulo">
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" name="paginaInicial" ${noticia?.paginaInicial ? 'checked' : ''}/>
                                Aparecer na pagina inicial
                            </label>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-12">
                        <div id="imagemListener" class="form-group is-fileinput">
                            <input name="featuredImageFile" type="file" class="form-control">

                            <div class="input-group">
                                <input type="text" readonly="" class="form-control" placeholder="Escolha uma imagem..">
                                <span class="input-group-btn input-group-sm">
                                    <button type="button" class="btn btn-fab btn-fab-mini">
                                        <i class="material-icons">attach_file</i>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Texto*</label>

                            <div class="form-group label-floating">
                                <label class="control-label">Escreva aqui as resposta para essa duvida</label>
                                    <ckeditor:editor id="ckeditor" name="mensagem" userSpace="${sec.loggedInUserInfo(field: 'id')}" height="400px">
                                    ${noticia?.mensagem}
                                    </ckeditor:editor>
                            </div>
                        </div>

                    </div>
                </div>

                <g:if test="${noticia != null}">
                    <input type="hidden" value="${noticia.id}" name="id"/>
                </g:if>
                <g:link action="index" type="button" class="btn btn-primary pull-left">Cancelar</g:link>
                <button type="submit" name="submitButton" class="btn btn-primary pull-right">Cadastrar</button>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div id="imagemNoticia">
    </div>
    <g:javascript type="text/javascript">
        $("#imagemListener").change(function () {
           loadImagem();
        });
        function getImageDiv(imageData){
            return "<div class='col-md-4'>" +
                        "<div class='card card-profile'>" +
                            "<div class='content'>" +
                                "<a href='#'>" +
                                    "<img class='img' height='300px' src='"+ imageData +"'>" +
                                "</a>" +
                            "</div>" +
                        "</div>" +
                    "</div> ";
        }

        function loadImagem() {
            var imageData = $("input[name='featuredImageFile']").prop('files')[0];
            if(imageData) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#imagemNoticia").html(getImageDiv(e.target.result));
                };
                reader.readAsDataURL(imageData);
            }
        }
    </g:javascript>
</g:form>
</body>
</html>