<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h3>${noticia.titulo}</h3>
        </div>
        <div class="card-content">
            ${raw(noticia.mensagem)}
        </div>
    </div>
</div>
<g:render template="/template/resposta" model="${[conteudo: noticia, isNoticia: true]}" />
</body>

</html>