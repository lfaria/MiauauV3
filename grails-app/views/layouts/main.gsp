<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <asset:link rel="apple-touch-icon" sizes="76x76" href="apple-icon.png" />
    <asset:link rel="icon" href="favicon.png" type="image/png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110054712-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-110054712-1');
    </script>


    <title>Miauau- Alguma coisa</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!--   Core JS Files   -->
    <asset:javascript  src="jquery-3.1.0.min.js" />
    <asset:javascript src="bootstrap.min.js" />
    <asset:javascript src="material.min.js" />

    <!--  Charts Plugin -->
    <asset:javascript src="chartist.min.js" />

    <!--  Notifications Plugin    -->
    <asset:javascript src="bootstrap-notify.js" />

    <!--  Google Maps Plugin    -->
    <asset:javascript  src="https://maps.googleapis.com/maps/api/js" />

    <!-- Material Dashboard javascript methods -->
    <asset:javascript src="material-dashboard.js" />
    <!-- Bootstrap core CSS     -->
    <asset:stylesheet src="bootstrap.css"  />

    <!--  Material Dashboard CSS    -->
    <asset:stylesheet src="material-dashboard.scss" />
    <asset:javascript src="miauau.js" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" >
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
    <g:layoutHead/>
</head>

<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="${assetPath(src:'sidebar-1.jpg')}">

        <!--
		        Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

		        Tip 2: you can also add an image using data-image tag
		    -->

        <div class="logo">
            <a href="/" class="simple-text">
                Miauau
            </a>
        </div>

        <div class="sidebar-wrapper">
            <ul class="nav">
                <li>
                    <g:link uri="/">
                        <i class="material-icons">dashboard</i>
                        <p>Dashboard</p>
                    </g:link>
                </li>
                <sec:ifNotLoggedIn>
                    <li>
                        <g:link controller="user" action="form">
                            <i class="material-icons">person</i>
                            <p>Cadastre-se!</p>
                        </g:link>
                    </li>
                </sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                    <li>
                        <g:link controller="aviso" action="meusAvisos">
                            <i class="material-icons">notifications</i>
                            <p>Meus Avisos</p>
                        </g:link>
                    </li>
                </sec:ifLoggedIn>
                <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_SYSADMIN">
                    <li>
                        <g:link controller="user" action="index">
                            <i class="material-icons">person-o</i>
                            <p>Controlar Contas</p>
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="duvida" action="duvidas">
                            <i class="material-icons">help outline</i>
                            <p>Controlar Duvidas</p>
                        </g:link>
                    </li>

                    <li>
                        <g:link controller="noticia">
                            <i class="material-icons">featured_play_list</i>
                            <p>Controlar Noticias</p>
                        </g:link>
                    </li>

                    <li>
                        <g:link controller="patrocinador">
                            <i class="material-icons">monetization_on</i>
                            <p>Controlar Patrocinadores</p>
                        </g:link>
                    </li>

                </sec:ifAnyGranted>
                <li>
                    <g:link controller="duvida" action="index">
                        <i class="material-icons">help</i>
                        <p>Duvidas?</p>
                    </g:link>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#principalNav">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="principalNav">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <g:link uri="/" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">dashboard</i>
                                <p class="hidden-lg hidden-md">Usuário</p>
                            </g:link>

                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="material-icons">notifications</i>
                                <span class="notification">5</span>
                                <p class="hidden-lg hidden-md">Notificações</p>
                            </a>
                            <ul class="dropdown-menu">
                                <!--
                                <li><a href="#">Mike John responded to your email</a></li>
                                <li><a href="#">You have 5 new tasks</a></li>
                                <li><a href="#">You're now friend with Andrew</a></li>
                                <li><a href="#">Another Notification</a></li>
                                <li><a href="#">Another One</a></li>
                                -->
                            </ul>
                        </li>

                        <sec:ifLoggedIn>
                            <li class="dropdown">
                                <a href="#"  class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Conta</p>
                                </a>
                                <ul class="dropdown-menu">
                                    <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_SYSADMIN">
                                        <li><g:link controller="user" action="form">Controlar Contas</g:link></li>
                                    </sec:ifAnyGranted>
                                    <li><g:link controller="user" action="form" id="${sec.loggedInUserInfo(field:'id')}">Editar conta</g:link></li>
                                    <li><g:link uri="/logoff" >Deslogar</g:link></li>
                                </ul>
                            </li>
                        </sec:ifLoggedIn>
                        <sec:ifNotLoggedIn>
                            <li>
                            <g:link controller="login" action="auth" class="dropdown-toggle">
                                <i class="material-icons">person</i>
                                <p class="hidden-lg hidden-md">Login</p>
                            </g:link>
                            </li>
                        </sec:ifNotLoggedIn>
                    </ul>

                    <form class="navbar-form navbar-right" role="search">
                        <div class="form-group  is-empty">
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="material-input"></span>
                        </div>
                        <button type="submit" class="btn btn-white btn-round btn-just-icon">
                            <i class="material-icons">search</i><div class="ripple-container"></div>
                        </button>
                    </form>
                </div>
            </div>
        </nav>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <g:layoutBody/>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <p class="category">Patrocinadores</p>
                        <g:each in="${miauau.rf07.Patrocinador.list()}">
                            <li>
                                <g:if test="${it.imagem}">
                                    <img height="50" src="${it.imagem.image}" alt="${it.nome}">
                                </g:if>
                                <g:else>
                                    ${it.nome}
                                </g:else>

                            </li>
                        </g:each>

                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Miauau</a>, feito com carinho para vc e seu bixinho
                </p>
            </div>
        </footer>
    </div>
</div>
<g:javascript>

    var url = window.location.pathname;
    $('a[href="'+ url + '"]').parent("li").attr("class", "active");

</g:javascript>
<g:if test="${flash.invalidObject}" >
    <g:javascript>
        var errors = "${flash.invalidObject}";
        backEnd.showDangerNotification("bottom", "right", errors);
    </g:javascript>
</g:if>

</body>




</html>