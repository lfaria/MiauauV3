<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Duvidas</title>
</head>
<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Duvidas</h4>
            <p class="category">Você pode visualizar e responder duvidas por aqui</p>
        </div>
        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-primary">
                <th>Nome</th>
                <th>Email</th>
                <th>Respondida</th>
                <th>Pergunta</th>
                <th>Responder</th>
                <th>Excluir</th>
                </thead>
                <tbody>
                <g:each in="${duvidas}">
                    <tr>
                        <td>${it.nome}</td>
                        <td>${it.email}</td>
                        <td>${it.resposta? "Sim" : "Não"}</td>
                        <g:if test="${it.mensagem.length() > 80}">
                            <td>${it.mensagem.substring(0, 80) + "..."}</td>
                        </g:if>
                        <g:else>
                            <td>${it.mensagem}</td>
                        </g:else>
                        <td>
                            <g:link action="responder" id="${it.id}">
                                <i class="material-icons">edit</i></g:link>

                        </td>
                        <td>
                            <g:link action="delete" id="${it.id}" onclick= "return confirm('Tem certeza?');">
                                <i class="material-icons">delete forever</i></g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>