<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Controlar Usuários</title>
</head>

<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Alguma duvida? Pergunta ae</h4>

            <p class="category">Você pode perguntas ou ver duvidas frequentes por aqui</p>
        </div>

        <div class="card-content">
            <g:form action="save">
                <sec:ifNotLoggedIn>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nome*</label>
                                <input class="form-control" name="nome"/>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Email*</label>
                                <input class="form-control" name="email"/>
                            </div>
                        </div>
                    </div>
                </sec:ifNotLoggedIn>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Duvida*</label>

                            <div class="form-group label-floating">
                                <label class="control-label">Escreva sua duvida aqui</label>
                                <textarea class="form-control" name="mensagem" rows="8"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <button name="submitButton" class="btn btn-primary pull-right" type="submit">Enviar</button>
            </g:form>
        </div>
    </div>
</div>

<div class="col-sm-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Duvidas frequentes</h4>

            <p class="category">Aqui você pode ver as duvidas frequentes</p>
        </div>

        <div class="card-content" name="duvidasFrequentes">
            <g:each in="${duvidasFrequentes}">
                <div class="row">
                    <div class="col-sm-12">
                        <h4>${it.mensagem}</h4>
                    </div>

                    <div class="col-sm-12">
                        <h4 style="padding-left:3em;">R: ${it.resposta}</h4>
                    </div>
                </div>
            </g:each>
        </div>
    </div>
</div>

</body>

</html>