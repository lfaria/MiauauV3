<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Responder Duvida</title>
</head>

<body>
<g:form method="post" action="saveResposta">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Respoda essa duvida</h4>

                <p class="category">Preencha os campos</p>
            </div>

            <div class="card-content">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <label>Pergunta*</label>

                            <div class="form-group label-floating">
                                <label class="control-label">Escreva aqui as resposta para essa duvida</label>
                                <textarea class="form-control" disabled="disabled" name="pergunta"
                                          rows="5">${duvida?.mensagem}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="checkbox">
                            <label class="control-label">
                                <input type="checkbox" name="frequente" ${duvida.frequente ? 'checked' : ''}/>
                                Frequente
                            </label>

                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Resposta*</label>

                            <div class="form-group label-floating">
                                <label class="control-label">Escreva aqui as resposta para essa duvida</label>
                                <textarea class="form-control" name="resposta" rows="5">${duvida?.resposta}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <g:if test="${duvida != null}">
                <input type="hidden" value="${duvida.id}" name="id"/>
            </g:if>
            <g:link action="duvidas" type="button" class="btn btn-primary pull-left">Cancelar</g:link>
            <button type="submit" name="submitButton" class="btn btn-primary pull-right">Responder</button>

            <div class="clearfix"></div>

        </div>
    </div>
</g:form>

</body>

</html>