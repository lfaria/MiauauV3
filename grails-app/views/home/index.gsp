<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="col-md-8">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h3>Noticias</h3>
        </div>
        <div class="card-content">
            <g:each in="${noticias}" status="i" var="it">
                <div id="${it.id}" class="col-sm-6">
                    <div class="card">
                        <div class="thumbnail">
                            <img src="${it.imagem?.image}"/>
                            <g:link controller="noticia" action="visualizar" id="${it.id}">${it.titulo}</g:link>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
    </div>

</div>

<div class="col-md-4">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Avisos Recentes</h4>

            <p class="category">Fique ligado!</p>
        </div>

        <div class="card-content">
            <g:each in="${avisos}">
                <div class="row">
                    <g:render template="/aviso/templates/avisoLateral"
                              model="${[imagem: it.imagem?.image, mensagem: it.mensagem, showLink: false, colSize: 12, email: it.usuario.email, id: it.id]}"/>
                </div>
            </g:each>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

</body>
</html>
