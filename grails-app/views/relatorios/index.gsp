<html>
<head>
    <meta name="layout" content="main" />
    <title>Relatórios</title>
</head>
<body>
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-chart" data-background-color="green">
                <div class="ct-chart" id="acessosDiarios"></div>
            </div>

            <div class="card-content">
                <h4 class="title">Acessos a noticias</h4>
            </div>
            <div class="card-footer">
                <i class="material-icons">access_time</i>
            </div>
        </div>
    </div>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header card-chart" data-background-color="green">
            <div class="ct-chart" id="acessosAvisos"></div>
        </div>

        <div class="card-content">
            <h4 class="title">Acessos a avisos</h4>
        </div>
        <div class="card-footer">
            <i class="material-icons">access_time</i>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="card">
        <div class="card-header card-chart" data-background-color="orange">
            <div class="ct-chart" id="comentariosNoticia"></div>
        </div>

        <div class="card-content">
            <h4 class="title">Comentários em noticias</h4>
        </div>
        <div class="card-footer">
            <i class="material-icons">access_time</i>
        </div>
    </div>
</div>
<div class="col-sm-6">
    <div class="card">
        <div class="card-header card-chart" data-background-color="orange">
            <div class="ct-chart" id="comentariosAvisos"></div>
        </div>

        <div class="card-content">
            <h4 class="title">Comentários em Avisos</h4>
        </div>
        <div class="card-footer">
            <i class="material-icons">access_time</i>
        </div>
    </div>
</div>

<g:javascript>
    $(document).ready(function () {
        backEnd.iniciaGraficoNoticas(JSON.parse("${noticias}"));
        backEnd.iniciaGraficoAvisos(JSON.parse("${avisos}"));
        backEnd.iniciaGraficoComentariosAvisos(JSON.parse("${comentariosAvisos}"));
        backEnd.iniciaGraficoComentariosNoticia(JSON.parse("${comentariosNoticias}"));
    });

</g:javascript>
</body>
</html>