<%@ page import="miauau.auth.Role" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Publicar Aviso</title>
</head>
<body>
<g:form method="post" action="save" enctype="multipart/form-data">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">${user? 'Edite seus dados' : 'Cadastre-se!'}</h4>
                <p class="category">Preencha os campos</p>
            </div>
            <div class="card-content">
                    <div class="row">
                        <div class="col-md-4">
                            <div class = "form-group">
                                <label class="control-label">Login*</label>
                                <input class="form-control" value="${user?.username}" name="username">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class = "form-group">
                                <label class="control-label">Email*</label>
                                <input class="form-control" value="${user?.email}" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class = "form-group">
                                <label class="control-label">Senha*</label>
                                <input type="password" class="form-control" value="${user?.password}" name="password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class = "form-group">
                                <label class="control-label">Confirme sua senha*</label>
                                <input type="password" value="${user?.password}" class="form-control" name="passwordCheck">
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class = "form-group">
                            <label class="control-label">Telefone</label>
                            <input type="text" class="form-control"  name="telefone">
                        </div>
                    </div>
                </div>
                    <input type="hidden" name="id" value="${user?.id}"/>
                <g:link action="index" type="button" class="btn btn-primary pull-left">Cancelar</g:link>
                    <button type="submit" name="loadForm" class="btn btn-primary pull-right">${user? 'Editar' : 'Cadastrar'}</button>
                    <div class="clearfix"></div>

            </div>
        </div>
    </div>
     <sec:ifAnyGranted roles="ROLE_ADMIN, ROLE_SYSADMIN">

        <div class="row">
            <g:render template="editUserRole" model="${[roleList: roleList, userRoles: user?.authorities]}"/>
        </div>
    </sec:ifAnyGranted>
</g:form>

</body>

</html>