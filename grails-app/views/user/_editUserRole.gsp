<div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Autoridades do usuário</h4>
            <p class="category">Você pode adicionar ou excluir autoridades por aqui</p>
        </div>
        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-primary">
                <th>Autoridade</th>
                <th>Ação</th>
                </thead>
                <tbody>
                <g:each status="i"  in="${roleList}" var="role">
                    <tr>
                        <td>${role.authority}</td>
                        <td>
                        <g:if test="${userRoles.find{role == it}}">
                            <button name="role${i}" type="button" onclick="toggleRoles($(this), ${role.id}, ${i})">
                                <i class="material-icons">remove</i>
                                <p>Remover</p>
                                <input type="hidden" value="${role.id}" name="role[${i}]">
                            </button>
                        </g:if>
                        <g:else>
                            <button type="button" name="role${i}" onclick="toggleRoles($(this), ${role.id}, ${i})">
                                <i class="material-icons">add</i>
                                <p>Adicionar</p>
                                <input type="hidden">
                            </button>
                        </g:else>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
        <button type="submit" class="btn btn-primary pull-right">${user? 'Editar' : 'Cadastrar'}</button>
    </div>
</div>
<g:javascript>
    function toggleRoles(button, id, index){
        if(button.find("input").val()){
            button.find("input").removeAttr("value");
            button.find("input").removeAttr("name");
            button.find("i").text("add");
            button.find("p").text("Adicionar");
        }else{
            button.find("input").val(id);
            button.find("input").attr("name", "role["+index+"]");
            button.find("i").text("remove");
            button.find("p").text("Remover");

        }
    }
</g:javascript>