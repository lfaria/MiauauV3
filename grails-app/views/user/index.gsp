<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Controlar Usuários</title>
</head>
<body>
<div class="col-md-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h4 class="title">Usuários</h4>
            <p class="category">Você pode editar ou excluir os usuários por aqui</p>
            <g:link action="form">
                <i class="material-icons">add</i>Cadastrar novo usuário</g:link>
        </div>
        <div class="card-content table-responsive">
            <table class="table">
                <thead class="text-primary">
                <th>Username</th>
                <th>Email</th>
                <th>Telefone</th>
                <th>Roles</th>
                <th>Editar</th>
                <th>Excluir</th>
                </thead>
                <tbody>
                <g:each in="${users}">
                    <tr>
                        <td>${it.username}</td>
                        <td>${it.email}</td>
                        <td>${it.telefone}</td>
                        <td>${it.authorities.collect{it.authority}}</td>

                        <td>
                            <g:link action="form" id="${it.id}">
                                <i class="material-icons">edit</i></g:link>

                        </td>
                        <td>
                            <g:link action="delete" id="${it.id}" onclick= "return confirm('Tem certeza? Isso vai deletar todos os avisos desse usuário');">
                                <i class="material-icons">delete forever</i></g:link>
                        </td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>

</html>