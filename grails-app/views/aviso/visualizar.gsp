<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="col-sm-12">
    <div class="card">
        <div class="card-header" data-background-color="green">
            <h3>Aviso de: ${aviso.usuario.email}</h3>
        </div>
        <div class="card-content">
            <h3>${raw(aviso.mensagem)}</h3>
        </div>
    </div>
</div>
<g:render template="/template/resposta" model="${[conteudo: aviso, isNoticia: false]}" />
</body>

</html>