<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Meus avisos</title>
</head>
<body>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">Meus avisos</h4>
                <p class="category">Você pode editar ou excluir seu avisos por aqui</p>
                <g:link action="form">
                    <i class="material-icons">add</i>Cadastrar novo Aviso</g:link>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <th>Id</th>
                    <th>Mensagem</th>
                    <th>Editar</th>
                    <th>Excluir</th>
                    </thead>
                    <tbody>
                    <g:each in="${avisos}">
                        <tr>
                            <td>${it.id}</td>
                            <g:if test="${it.mensagem.length() > 80}">
                                <td>${it.mensagem.substring(0, 80) + "..."}</td>
                            </g:if>
                            <g:else>
                                <td>${it.mensagem}</td>
                            </g:else>
                            <td>
                                <g:link action="form" id="${it.id}">
                                    <i class="material-icons">edit</i></g:link>

                            </td>
                            <td>
                                <g:link action="delete" id="${it.id}" onclick= "return confirm('Tem certeza?');">
                                    <i class="material-icons">delete forever</i></g:link>
                            </td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>