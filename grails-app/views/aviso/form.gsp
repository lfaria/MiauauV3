<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main.gsp"/>
    <title>Publicar Aviso</title>
</head>
<body>
<g:form method="post" action="save" enctype="multipart/form-data">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">${aviso? 'Edite' : 'Cadastre'} um aviso</h4>
                <p class="category">Preencha os campos</p>
            </div>
            <div class="card-content">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group is-fileinput">
                                <input name="featuredImageFile" type="file" class="form-control" >
                                <div class="input-group">
                                    <input type="text" readonly="" class="form-control" placeholder="Escolha uma imagem..">
                                    <span class="input-group-btn input-group-sm">
                                        <button type="button" class="btn btn-fab btn-fab-mini">
                                            <i class="material-icons">attach_file</i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Mensagem*</label>
                                <div class="form-group label-floating">
                                    <label class="control-label"> Escreva aqui as informações sobre seu animal, como nome, ultimo lugar visto</label>
                                    <textarea class="form-control" name="mensagem" rows="5">${aviso?.mensagem}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <g:if test="${aviso != null}">
                        <input type="hidden" value="${aviso.id}" name="id" />
                    </g:if>
                <g:link action="meusAvisos" type="button" class="btn btn-primary pull-left">Cancelar</g:link>
                    <button type="button" name="loadButton" onclick="loadAvisoLateral();" class="btn btn-primary pull-right">Visualizar</button>
                    <div class="clearfix"></div>

            </div>
        </div>
    </div>
    <div id = "avisoLateral">
        <g:if test="${aviso != null}">
            <g:render template="templates/avisoLateral" model="${[imagem: aviso.imagem?.image, mensagem: aviso.mensagem, showLink: true, colSize: 4, email: aviso.usuario.email, id: aviso.id]}" />
        </g:if>
    </div>
    <g:javascript type="text/javascript">

        function loadAvisoLateral() {
            var mensagem = $("textarea[name='mensagem']").val();
            var imageData = $("input[name='featuredImageFile']").prop('files')[0];
            if(imageData) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    var data = {
                        mensagem: mensagem,
                        imageData: e.target.result,
                        showLink: true
                    };
                    $.ajax({
                        method: 'POST',
                        url: "/aviso/loadAvisoLateral",
                        data: data,
                        success: function (val) {
                            $("#avisoLateral").html(val);
                        }
                    });
                };
                reader.readAsDataURL(imageData);
            }else{
                var data = {
                    mensagem: mensagem,
                    imageData: "",
                    showLink: true
                };
                $.ajax({
                    method: 'POST',
                    url: "/aviso/loadAvisoLateral",
                    data: data,
                    success: function (val) {
                        $("#avisoLateral").html(val);
                    }
                });
            }

        }
    </g:javascript>

</g:form>


</body>

</html>