<div class="col-md-${colSize}">
    <div class="card card-profile">
        <div class="card-avatar">
            <a href="#">
                <img class="img" height="300px" src="${imagem}"/>
            </a>
        </div>

        <div class="content">
            <h6 class="category text-gray">Por:${email?: ''} </h6>
            <h4 class="card-title"><g:link controller="aviso" action="visualizar" id="${id}">Mensagem</g:link></h4>
            <p class="card-content">
                ${mensagem}
            </p>
            <g:if test="${showLink}">
                <button name="uploadButton" type='submit' class='btn btn-primary btn-round'>Publicar</button>
            </g:if>
        </div>
    </div>
</div>