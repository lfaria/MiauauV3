
// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'miauau.auth.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'miauau.auth.UserRole'
grails.plugin.springsecurity.authority.className = 'miauau.auth.Role'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/ck/standard/filemanager/**',access: ['permitAll']],
	[pattern: '/ck/standard/uploader/**',			 access: ['permitAll']],
	[pattern: '/ck/ofm/filemanager/**',			 access: ['permitAll']],
	[pattern: '/ck/ofm/filetree/**',			 access: ['permitAll']],
	[pattern: '/uploads/**',			 access: ['permitAll']],
	[pattern: '/ck/**',			 access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']]
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/**',             filters: 'JOINED_FILTERS']
]
ckeditor {
	config = "/js/myckconfig.js"
	skipAllowedItemsCheck = false
	defaultFileBrowser = "ofm"
	upload {
		basedir = "/uploads/"
		overwrite = false
		link {
			browser = true
			upload = false
			allowed = []
			denied = ['html', 'htm', 'php', 'php2', 'php3', 'php4', 'php5',
					  'phtml', 'pwml', 'inc', 'asp', 'aspx', 'ascx', 'jsp',
					  'cfm', 'cfc', 'pl', 'bat', 'exe', 'com', 'dll', 'vbs', 'js', 'reg',
					  'cgi', 'htaccess', 'asis', 'sh', 'shtml', 'shtm', 'phtm']
		}
		image {
			browser = true
			upload = true
			allowed = ['jpg', 'gif', 'jpeg', 'png']
			denied = []
		}
		flash {
			browser = false
			upload = false
			allowed = ['swf']
			denied = []
		}
	}
}
