var backEnd = {
    notificationsColors: ['info', 'success', 'warning', 'danger'],
    showNotification: function (from, align, message, color) {

        $.notify({
            icon: "notifications",
            message: message

        }, {
            type: color,
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
    },
    showInfoNotification: function (from, align, message) {
        this.showNotification(from, align, message, this.notificationsColors[0]);
    },
    showSuccessNotification: function (from, align, message) {
        this.showNotification(from, align, message, this.notificationsColors[1]);
    },
    showWarningNotification: function (from, align, message) {
        this.showNotification(from, align, message, this.notificationsColors[2]);
    },
    showDangerNotification: function (from, align, message) {
        this.showNotification(from, align, message, this.notificationsColors[3]);
    },

    iniciaGraficoNoticas: function (jsonData) {
        dataAcessosDiarios = {
            labels: ['Seg', 'Ter', 'Quar', 'Quin', 'Sex', 'Sab', 'Dom'],
            series: [
                [
                    jsonData.Seg? jsonData.seg : 0,
                    jsonData.Ter? jsonData.Ter: 0,
                    jsonData.Qua? jsonData.Qua: 0,
                    jsonData.Qui? jsonData.Qui : 0,
                    jsonData.Sex? jsonData.Sex: 0,
                    jsonData.Sab? jsonData.Sab: 0,
                    jsonData.Dom? jsonData.Dom: 0

                ]
            ]
        };
        optionsAcessosDiarios = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 100, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
        };

        var acessosDiarios = new Chartist.Line('#acessosDiarios', dataAcessosDiarios, optionsAcessosDiarios);

        md.startAnimationForLineChart(acessosDiarios);
    },
    iniciaGraficoAvisos: function (jsonData) {
        dataAcessosDiarios = {
            labels: ['Seg', 'Ter', 'Quar', 'Quin', 'Sex', 'Sab', 'Dom'],
            series: [
                [
                    jsonData.Seg? jsonData.seg : 0,
                    jsonData.Ter? jsonData.Ter: 0,
                    jsonData.Qua? jsonData.Qua: 0,
                    jsonData.Qui? jsonData.Qui : 0,
                    jsonData.Sex? jsonData.Sex: 0,
                    jsonData.Sab? jsonData.Sab: 0,
                    jsonData.Dom? jsonData.Dom: 0

                ]
            ]
        };
        optionsAcessosDiarios = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 100, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
        };

        var acessosDiariosAvisos = new Chartist.Line('#acessosAvisos', dataAcessosDiarios, optionsAcessosDiarios);

        md.startAnimationForLineChart(acessosDiariosAvisos);
    },

    iniciaGraficoComentariosNoticia: function (jsonData) {
        dataAcessosDiarios = {
            labels: ['Seg', 'Ter', 'Quar', 'Quin', 'Sex', 'Sab', 'Dom'],
            series: [
                [
                    jsonData.Seg? jsonData.seg : 0,
                    jsonData.Ter? jsonData.Ter: 0,
                    jsonData.Qua? jsonData.Qua: 0,
                    jsonData.Qui? jsonData.Qui : 0,
                    jsonData.Sex? jsonData.Sex: 0,
                    jsonData.Sab? jsonData.Sab: 0,
                    jsonData.Dom? jsonData.Dom: 0

                ]
            ]
        };
        optionsAcessosDiarios = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 100, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
        };

        var acessosDiariosAvisos = new Chartist.Line('#comentariosNoticia', dataAcessosDiarios, optionsAcessosDiarios);

        md.startAnimationForLineChart(acessosDiariosAvisos);
    },
    iniciaGraficoComentariosAvisos: function (jsonData) {
        dataAcessosDiarios = {
            labels: ['Seg', 'Ter', 'Quar', 'Quin', 'Sex', 'Sab', 'Dom'],
            series: [
                [
                    jsonData.Seg? jsonData.seg : 0,
                    jsonData.Ter? jsonData.Ter: 0,
                    jsonData.Qua? jsonData.Qua: 0,
                    jsonData.Qui? jsonData.Qui : 0,
                    jsonData.Sex? jsonData.Sex: 0,
                    jsonData.Sab? jsonData.Sab: 0,
                    jsonData.Dom? jsonData.Dom: 0

                ]
            ]
        };
        optionsAcessosDiarios = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: 100, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: {top: 0, right: 0, bottom: 0, left: 0}
        };

        var acessosDiariosAvisos = new Chartist.Line('#comentariosAvisos', dataAcessosDiarios, optionsAcessosDiarios);

        md.startAnimationForLineChart(acessosDiariosAvisos);
    }


};