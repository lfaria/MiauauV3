package miauauv3

import grails.transaction.Transactional
import grails.web.mapping.LinkGenerator
import miauau.auth.User
import miauau.rf01.Imagem
import org.springframework.web.multipart.MultipartFile

@Transactional
class ImageService {
	def grailsLinkGenerator

	def saveImage(MultipartFile imageData, User user, Imagem img = null) {
		if (!imageData || imageData.isEmpty())
			return null
		if (!img)
			img = new Imagem()
		img.bytes = imageData.bytes
		img.contentType = imageData.getContentType()
		img.save()
		img
	}

	def getImage(Long imgId) {
		grailsLinkGenerator.link(controller: 'imagem', action: 'renderImage', id: imgId)
	}

}
