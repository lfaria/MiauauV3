package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import miauau.Counter
import miauau.rf05.Noticia
import org.springframework.http.HttpStatus

@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN"])
class NoticiaController {
	def springSecurityService
	def imageService
	def index() {
		[noticias: Noticia.list()]
	}

	def form(Noticia noticia){
		[noticia: noticia ?: flash.chainModel?.noticia]
	}

	def save(Noticia noticia, ImagemCommand imagem) {
		noticia.usuario = springSecurityService.getCurrentUser()
		noticia.validate()
		if (noticia.hasErrors()) {
			flash.invalidObject = message(error: noticia.errors.getFieldError()).toString()
			chain action: 'form', model: [noticia: noticia]
			return
		}

		imagem.validate()
		if(imagem.hasErrors()){
			flash.invalidObject = message(error: imagem.errors.getFieldError()).toString()
			chain action: 'form', model: [noticia: noticia]
		}
		def img = imageService.saveImage(imagem.featuredImageFile, noticia.usuario, noticia.imagem)
		if(img)
			noticia.imagem = img
		noticia.save(flush: true, failOnError: true)
		redirect action: 'index'
	}

	def delete(Noticia noticia){
		if(noticia){
			noticia.imagem?.delete()
			noticia.delete(flush: true)
		}
		redirect action: 'index'
	}

	@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
	def visualizar(Noticia n){
		new Counter(noticia: true, dataAcessado: new Date()).save(flush: true)
		[noticia: n]
	}
}
