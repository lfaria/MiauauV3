package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import miauau.rf01.Imagem

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class ImagemController {

	def renderImage(Imagem img) {
		if (!img || img.bytes == null) {
			render status: 404
			return
		}
		render file: img.bytes, contentType: img.contentType
	}
}
