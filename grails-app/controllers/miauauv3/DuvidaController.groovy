package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import miauau.auth.User
import miauau.br.com.miauau.rf03.Duvida

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class DuvidaController {

	def index() {
		[duvidasFrequentes: Duvida.findAllByFrequente(true)]
	}

	@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN"])
	def duvidas() {
		def duvidas = Duvida.listOrderByDataResposta(order: 'asc')
		[duvidas: duvidas]
	}

	@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN"])
	def delete() {
		if(params.id){
			Duvida.get(params.id).delete(flush: true)
		}
		redirect action: 'duvidas'
	}


	@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN"])
	def responder() {
		if (params.id)
			[duvida: Duvida.get(params.id)]
		else
			redirect action: 'duvidas'
	}

	@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN"])
	def saveResposta() {
		if (params.resposta && params.id) {
			Duvida d = Duvida.get(params.id)
			d.resposta = params.resposta
			d.dataResposta = new Date()
			d.frequente = params.frequente as boolean
			d.save(flush: true)
		} else if (!params.resposta && params.id) {
			flash.invalidObject = "Você deve fornecer uma resposta para essa duvida"
			redirect action: 'responder', id: params.id
			return
		}
		redirect action: 'duvidas'
	}

	def save(DuvidaCommand cmd) {
		if (cmd.hasErrors()) {
			flash.invalidObject = message(error: cmd.errors.getFieldError()).toString()
			redirect action: 'index'
			return
		}
		Duvida d = cmd.duvida
		d.save()
		render view: 'aguarde'
	}

}

class DuvidaCommand implements Validateable {
	def springSecurityService
	String mensagem
	String nome
	String email
	User usuario

	static constraints = {
		importFrom Duvida
		email validator: { val, obj ->
			if (obj.springSecurityService.getCurrentUser() && (!val || val.isEmpty()))
				return true
			else if (!val || val.isEmpty())
				return false
		}
		nome validator: { val, obj ->
			if (obj.springSecurityService.getCurrentUser() && (!val || val.isEmpty()))
				return true
			else if (!val || val.isEmpty())
				return false
		}
	}

	def getDuvida() {
		Duvida d = new Duvida()
		if (this.springSecurityService.getCurrentUser()) {
			d.usuario = this.springSecurityService.getCurrentUser()
			d.email = d.usuario.email
			d.nome = d.usuario.username
		} else {
			d.email = this.email
			d.nome = this.nome
		}
		d.mensagem = this.mensagem
		d

	}

}