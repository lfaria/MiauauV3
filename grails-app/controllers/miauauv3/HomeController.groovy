package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import miauau.rf01.Aviso
import miauau.rf05.Noticia

@Secured("IS_AUTHENTICATED_ANONYMOUSLY")
class HomeController {

	def index() {
		[avisos: Aviso.listOrderById(order: 'desc'), noticias: Noticia.findAllByPaginaInicial(true)]
	}
}
