package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import miauau.rf07.Patrocinador

@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN"])
class PatrocinadorController {
	def imageService

	def index() {
		[patrocinadores: Patrocinador.list()]
	}

	def form(Patrocinador p) {
		[patrocinador: p ?: flash.chainModel?.patrocinador]
	}

	def save(Patrocinador p, ImagemCommand imagemCommand) {
		p.validate()
		if (p.hasErrors()) {
			flash.invalidObject = message(error: p.errors.getFieldError()).toString()
			chain action: 'form', model: [patrocinador: p]
		}
		imagemCommand.validate()
		if (imagemCommand.hasErrors()) {
			flash.invalidObject = message(error: imagemCommand.errors.getFieldError()).toString()
			chain action: 'form', model: [patrocinador: p]
		}
		def img = imageService.saveImage(imagemCommand.featuredImageFile, null, p.imagem)
		if (img)
			p.imagem = img
		p.save(flush: true, failOnError: true)
		redirect action: ' index'


	}

	def delete(Patrocinador p) {
		p.imagem.delete()
		p.delete(flush: true, failOnError: true)
		redirect action: 'index'
	}


}
