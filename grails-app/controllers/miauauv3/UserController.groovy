package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole
import miauau.rf01.Imagem
import miauau.rf01.Aviso
import miauau.rf05.Noticia
import org.springframework.http.HttpStatus

@Secured(['ROLE_USER', 'ROLE_ADMIN', 'ROLE_SYSADMIN'])
class UserController {

	def springSecurityService

	@Secured(['ROLE_ADMIN', 'ROLE_SYSADMIN'])
	def index() {
		[users: User.list()]
	}

	@Secured(['ROLE_ADMIN', 'ROLE_SYSADMIN'])
	def delete(User u) {
		User.withTransaction {
			UserRole.removeAll(u)
			Aviso.findAllByUsuario(u)*.delete()
			Noticia.findAllByUsuario(u)*.delete()
			u.delete(flush: true)
		}
		redirect action: 'index'
	}

	@Secured('IS_AUTHENTICATED_ANONYMOUSLY')
	def form(User user) {
		def currentUserAuthorities = springSecurityService.getCurrentUser()?.authorities
		if ((user && user != springSecurityService.getCurrentUser()))
			if (!currentUserAuthorities.find { it.authority == 'ROLE_ADMIN' || it.authority == 'ROLE_SYSADMIN' }) {
				render status: HttpStatus.FORBIDDEN
				return
			}
		render view: 'form', model: [user: user?: flash.chainModel?.user, roleList: Role.list()]
	}

	@Secured('IS_AUTHENTICATED_ANONYMOUSLY')
	def save(UserCommand userCommand) {
		userCommand.validate()
		if (userCommand.hasErrors()) {
			flash.invalidObject = message(error: userCommand.errors.getFieldError()).toString()
			chain action: 'form', id: "${userCommand.id ?: ''}", model: [user: userCommand.user]
			return
		}

		def user
		if (!userCommand.id)
			user = userCommand.user
		else
			user = User.get(userCommand.id)
		user.validate()
		if(user.hasErrors()) {
			flash.invalidObject = message(error: user.errors.getFieldError()).toString()
			chain action: 'form', id: "${userCommand.id ?: ''}", model: [user: user]
			return
		}
		user.save(failOnError: true, flush: true)
		if (userCommand.role) {
			if(UserRole.removeAll(user))
				userCommand.role.each {
					UserRole.create(user, Role.get(it as Long), true)
				}
		} else if (!UserRole.get(user.id, Role.findByAuthority('ROLE_USER').id))
			UserRole.create(user, Role.findByAuthority('ROLE_USER'), true)


		if (springSecurityService.getCurrentUser()?.authorities?.find {
			it.authority == 'ROLE_ADMIN' || it.authority == 'ROLE_SYSADMIN'
		}) {
			redirect controller: 'user', action: 'index'
			return
		}
		redirect controller: 'login', action: "auth"
	}
}


class UserCommand implements Validateable {
	def springSecurityService
	Long id
	String username
	String password
	String passwordCheck
	String email
	String telefone
	List<String> role

	static constraints = {
		importFrom(User)
		role nullable: true, validator: { val, obj ->
			def senderUserAuthorities = obj.springSecurityService.getCurrentUser()?.authorities
			boolean isAdmin = senderUserAuthorities.find{it.authority == 'ROLE_ADMIN' || it.authority == 'ROLE_SYSADMIN'} as boolean
			if(val && !isAdmin)
				return false
			if(!val && isAdmin)
				return 'deveTerRole'

		}
		id nullable: true
		password validator: { val, obj ->
			obj.passwordCheck == val ?: 'message'
		}
	}

	User getUser() {
		User user = id ? User.get(id) : new User()
		user.properties = this.properties
	}
}
