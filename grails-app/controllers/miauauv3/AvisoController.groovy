package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import grails.validation.Validateable
import miauau.Counter
import miauau.rf01.Aviso
import org.springframework.http.HttpStatus
import org.springframework.web.multipart.MultipartFile

@Secured(["ROLE_ADMIN", "ROLE_SYSADMIN", "ROLE_USER"])

class AvisoController {
	def imageService
	def springSecurityService

	def index() {

	}

	def meusAvisos() {
		def avisos = Aviso.findAllByUsuario(springSecurityService.getCurrentUser())
		render view: 'meusAvisos', model: [avisos: avisos]
	}

	def form(Aviso aviso) {
		render view: 'form', model: [aviso: aviso?: flash.chainModel?.aviso]
	}

	def loadAvisoLateral() {
		render template: "templates/avisoLateral", model: [imagem: params.imageData, mensagem: params.mensagem, showLink: params.showLink.toBoolean(), colSize: 4]
	}

	def save(Aviso novoAviso, ImagemCommand imagem) {
		if (novoAviso.usuario == springSecurityService.getCurrentUser() || novoAviso.usuario == null) {
			if (imagem.hasErrors()) {
				flash.invalidObject = message(error: imagem.errors.getFieldError()).toString()
				chain action: 'form', model:[aviso: novoAviso]
				return
			}
			try {
				def user = springSecurityService.getCurrentUser()
				Aviso.withTransaction { transaction ->
					if (!imagem.featuredImageFile.isEmpty()) {
						def publicationImage = imageService.saveImage(imagem.featuredImageFile, user, novoAviso.imagem)
						novoAviso.imagem = publicationImage
					}
					novoAviso.usuario = user
					novoAviso.validate()
					if (novoAviso.hasErrors()) {
						transaction.setRollbackOnly()
						flash.invalidObject = message(error: novoAviso.errors.getFieldError()).toString()
						chain action: 'form', model:[aviso: novoAviso]
						return
					}
					novoAviso.save(flush: true)
					redirect action: 'meusAvisos'
				}
			} catch (Exception e) {
				println e.getMessage()
				redirect action: 'form'
			}
		} else {
			render status: HttpStatus.FORBIDDEN
		}

	}

	def delete(Aviso aviso) {
		if (aviso) {
			aviso.imagem.delete()
			aviso.delete(flush: true)
		}
		redirect action: 'meusAvisos'
	}

	def visualizar(Aviso aviso) {
		new Counter(noticia: false, dataAcessado: new Date()).save(flush: true)
		[aviso: aviso]
	}

}

class ImagemCommand implements Validateable {
	MultipartFile featuredImageFile

	static constraints = {
		featuredImageFile validator: { val, obj ->
			if (val == null) {
				return true
			}
			if (val.empty) {
				return true
			}

			['jpeg', 'jpg', 'png'].any { extension ->
				val.contentType?.toLowerCase()?.endsWith(extension)
			}
		}
	}
}
