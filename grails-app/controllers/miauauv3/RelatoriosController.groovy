package miauauv3

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import miauau.Counter
import miauau.rf01.Aviso
import miauau.rf05.Noticia
import miauau.rf05.Resposta


@Secured(['ROLE_ADMIN', 'ROLE_SYSADMIN'])
class RelatoriosController {

	def index() {
		def noticias = [:]
		Counter.findAllByNoticia(true).each {
			def dia = Date.parse("dd/MM/yyyy ", it.dataAcessado.toLocaleString()).format('EEE')
			if (noticias[dia] != null)
				noticias[dia] += 1
			else
				noticias[dia] = 1
		}
		def avisos = [:]
		Counter.findAllByNoticia(false).each {
			def dia = Date.parse("dd/MM/yyyy ", it.dataAcessado.toLocaleString()).format('EEE')
			if (avisos[dia] != null)
				avisos[dia] += 1
			else
				avisos[dia] = 1
		}

		def comentariosNoticias = [:]
		Noticia.list().each {
			it.resposta.each { resp->
				def dia = Date.parse("dd/MM/yyyy ", resp.dateCreated.toLocaleString()).format('EEE')
				if (comentariosNoticias[dia] != null)
					comentariosNoticias[dia] += 1
				else
					comentariosNoticias[dia] = 1
			}
		}

		def comentariosAvisos = [:]
		Aviso.list().each {
			it.resposta.each { resp->
				def dia = Date.parse("dd/MM/yyyy ", resp.dateCreated.toLocaleString()).format('EEE')
				if (comentariosAvisos[dia] != null)
					comentariosAvisos[dia] += 1
				else
					comentariosAvisos[dia] = 1
			}
		}
		println comentariosAvisos
		println comentariosNoticias

		[noticias: noticias as JSON, avisos: avisos as JSON, comentariosAvisos: comentariosAvisos as JSON, comentariosNoticias: comentariosNoticias as JSON]

	}
}
