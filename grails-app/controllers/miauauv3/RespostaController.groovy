package miauauv3

import grails.plugin.springsecurity.annotation.Secured
import miauau.rf01.Aviso
import miauau.rf05.Noticia
import miauau.rf05.Resposta

@Secured(['ROLE_ADMIN', 'ROLE_SYSADMIN', 'ROLE_USER'])
class RespostaController {
	def springSecurityService

	def adicionarResposta() {
		def conteudo
		if(params.isNoticia.toBoolean())
			conteudo = Noticia.get(params.id)
		else
			conteudo = Aviso.get(params.id)
		def resposta
		if(params.respostaId)
			resposta = Resposta.get(params.respostaId)
		else
			resposta = new Resposta()

		resposta.usuario = springSecurityService.getCurrentUser()
		resposta.mensagem = params.mensagem
		//if(resposta.hasErrors()){}
		conteudo.addToResposta(resposta)
		conteudo.save(flush: true, failOnError: true)

		redirect controller: params.isNoticia.toBoolean()? 'noticia' : 'aviso', action: 'visualizar', id: params.id


	}

	def deletar() {
		def conteudo
		if(params.isNoticia.toBoolean())
			conteudo = Noticia.get(params.id)
		else
			conteudo = Aviso.get(params.id)
		conteudo.removeFromResposta(Resposta.get(params.noticiaId)).save(flush:true, failOnError: true)
		redirect controller: params.isNoticia.toBoolean()? 'noticia' : 'aviso', action: 'visualizar', id: params.id

	}
}
