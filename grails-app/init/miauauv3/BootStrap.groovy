package miauauv3

import miauau.auth.Role
import miauau.auth.User
import miauau.auth.UserRole

class BootStrap {

	def init = { servletContext ->
		def usersAndRoles = [
				[username: 'sysAdmin', email: 'sysAdmin@miauau.com', role: 'ROLE_SYSADMIN'],
				[username: 'normalUser', email: 'normalUser@miauau.com', role: 'ROLE_USER'],
				[username: 'admin', email: 'admin@miauau.com', role: 'ROLE_ADMIN']
		]
		def listRoles = Role.list().collect { it.authority }
		usersAndRoles.eachWithIndex { it, index ->
			if (!listRoles.contains(it.role))
				new Role(authority: it.role).save(flush: true)

			def user = User.findByUsername(it.username)
			if (!user) {
				user = new User(username: it.username, password: "123", email: it.email)
				user.save(failOnError: true)
				UserRole.create(user, Role.findByAuthority(it.role), true)
			} else {
				def role = Role.findByAuthority(it.role)
				if (!user.getAuthorities().contains(role))
					UserRole.create(user, role, true)
			}
		}
	}
	def destroy = {
	}
}
